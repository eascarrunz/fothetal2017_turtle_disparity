# README #

This repository contains the data and R scripts necessary for producing the skull disparity curves of the manuscript "Still slow, but even steadier: An update on the evolution of turtle cranial disparity interpolating shapes along branches and adjusting for topological and temporal uncertainty" by Foth et al.

### Reproducing the curve of Figure 1a ###

You will need to install the following packages:

- `paleotree`
- `phangorn`
- `Rphylopars`

Then, the scripts need to be run in the following order:

1. `scripts/scale_trees_pereira.R` - This produces time-scaled trees from a base supertree, information about the ages of fossils, and molecular node ages from Pereira et al. (2017). It contains information on how to change the parameters of the `bin_timePaleoPhy` function in order to produce the three sets of time-scaled trees used in the manuscript. 

2. `scripts/interpol_edge.R` - This maps the shape variables onto the nodes of trees, interpolates those values along tree edges ("branches"), and computes the sum of variances for each time bin. The script `scripts/interpol_edge_recent.R` does the same, but excluding data from fossils.

3. `scripts/make_plots.R` - Produces a simple version of the plot used for Fig 1a, and can be used with minor modifications to produce almost all the other plots, which are written to the `figs` directory. For producing the plot with the Pan-cryptodire and Pan-pleurodire, use `scripts/make_plots_clades.R`.